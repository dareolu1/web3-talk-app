import { render, cleanup } from "@testing-library/react";
import Report from "../pages/Report";
import { MoralisProvider } from "react-moralis";
import { Provider } from "react-redux";
import store from "../hooks/store";

afterEach(cleanup);

test("renders learn react link", () => {
  const { container } = render(
    <MoralisProvider
      serverUrl="https://2yt6qqu9bbig.usemoralis.com:2053/server"
      appId="glu0UxawhVFkrV6z3baLGmTM6578uZKBO51jJjkl"
    >
      <Provider store={store}>
        <Report />
      </Provider>
    </MoralisProvider>,
  );
  expect(container).toMatchSnapshot();
});
