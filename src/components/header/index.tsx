import React from "react";
import { NavLink } from "react-router-dom";
import Account from "./Account/Account";

export default function Header() {
  return (
    <header className="page-header">
      <div className="logo-text" style={{ cursor: "pointer" }}>
        <NavLink to="/">
          <h3 className="app-symbol">Web3 Talk</h3>
        </NavLink>
      </div>
      <nav className="navbar">
        <ul className="nav-list">
          {links.map((link) => (
            <li key={link.label} className="link listitem active">
              <NavLink
                to={link.to}
                className={({ isActive }) => (isActive ? "active" : undefined)}
              >
                {link.label}
              </NavLink>
            </li>
          ))}
        </ul>
      </nav>
      <div className="header-right">
        <div className="menu">
          <Account />
        </div>
      </div>
    </header>
  );
}

const links = [
  {
    label: "Report",
    to: "/report",
  },
  {
    label: "Profile",
    to: "/profile",
  },
  {
    label: "Payment",
    to: "/payment",
  },
];
