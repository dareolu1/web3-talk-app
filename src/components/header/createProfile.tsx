import { useMoralis } from "react-moralis";
import React, { SetStateAction } from "react";
import "../../styles/profile-edit.css";
import { useForm } from "react-hook-form";
import { Button, Drawer, Typography, Input, Select } from "antd";
import codes from "../../assets/codes";
import notification from "../notification";
import { useAppDispatch } from "../../hooks/store";
import { authUser, User } from "../../hooks/reducer";

const { Option } = Select;

const selectBefore = "https://";

export type FormData = {
  fullname: string;
  email: string;
  phone: string;
  role: string;
  github: string;
  linkedin: string;
  gitlab: string;
  location: string;
  instagram: string;
  twitter: string;
  password: string;
  gender: string;
  username: string;
  usdtWalletAddress: string;
};

type Props = {
  isAuthDrawerVisible: boolean;
  setIsAuthDrawerVisible: React.Dispatch<SetStateAction<boolean>>;
};

export default function CreateProfile(props: Props) {
  const { isAuthDrawerVisible = true, setIsAuthDrawerVisible } = props;
  const [creating, setCreating] = React.useState<boolean>(false);
  const { user } = useMoralis();
  const dispatch = useAppDispatch();
  const [prefix, setPrefix] = React.useState<string>("+44");
  const {
    handleSubmit,
    register,
    setValue,
    formState: { errors },
    reset,
    watch,
  } = useForm<FormData>();

  const {
    fullname,
    email,
    phone,
    role,
    github,
    linkedin,
    gitlab,
    location,
    instagram,
    twitter,
    password,
    gender,
    username,
    usdtWalletAddress,
  } = watch();

  const Submit = (data: FormData) => {
    if (creating) return;

    try {
      setCreating(true);
      const socialLinks = [
        "linkedin",
        "instagram",
        "gitlab",
        "github",
        "twitter",
      ] as const;
      type SOCIAL = typeof socialLinks[number];

      let social: Partial<Pick<FormData, SOCIAL>> = {};

      for (const key in data) {
        if (socialLinks.includes(key as SOCIAL)) {
          social[key as SOCIAL] = data[key as keyof FormData];
        } else {
          user?.set(key, data[key as keyof FormData]);
        }
      }

      user?.set("social", social);
      user?.save();

      reset();
      setCreating(false);

      const currentUser = user?.toJSON() as unknown as User;
      dispatch(authUser(currentUser));

      const title = "Profile Created Successfuly";
      const description = (
        <Typography.Text>
          Welcome to Web3 Talk Team{" "}
          <b style={{ color: "var(--text-secondary)" }}>{data.fullname}</b>. We
          are happy to have you here
          <Typography.Paragraph strong>
            (greetings from the team)
          </Typography.Paragraph>
        </Typography.Text>
      );

      notification("success", title, description, "topRight");
    } catch (error) {
      console.error(error);
      setCreating(false);
    }
  };

  const handleChange = (key: keyof FormData, value: string) => {
    setValue(key, value);
  };

  const PhoneCodes = (
    <Select defaultValue={"+44"} onChange={(e) => setPrefix(e)}>
      {codes.map((country) => (
        <Option value={country.dialCode} key={country.name}>
          <h5>
            {country.iso} {country.dialCode}
          </h5>
        </Option>
      ))}
    </Select>
  );

  return (
    <Drawer
      title={
        <div>
          <Typography.Title
            level={5}
            style={{ color: "var(--primary-color)", fontWeight: 700 }}
          >
            Create Profile
          </Typography.Title>
          <Typography.Text style={{ fontSize: "small" }}>
            We create a profile for every connected wallet, please create a
            profile to enjoy the feature of the app
          </Typography.Text>
        </div>
      }
      placement="right"
      size={"default"}
      drawerStyle={{ background: "#f2f2f2" }}
      headerStyle={{ background: "#f2f2f2" }}
      onClose={() => setIsAuthDrawerVisible(false)}
      visible={isAuthDrawerVisible}
    >
      <main className="main-content">
        <div className="form-container">
          <form action="#" className="form" onSubmit={handleSubmit(Submit)}>
            <div className="user-details section">
              <div className="form-group">
                <label htmlFor="fullName">
                  <span className="details">
                    Full Name<b style={{ color: "red" }}>*</b>
                  </span>
                </label>
                <Input
                  type="text"
                  id="fullName"
                  placeholder="Enter your full name"
                  className="form-control"
                  value={fullname ?? ""}
                  status={errors.fullname ? "error" : undefined}
                  {...register("fullname", {
                    required: true,
                    onChange: (e) => handleChange("fullname", e.target.value),
                  })}
                />
              </div>
              <div className="form-group">
                <label htmlFor="fullName">
                  <span className="details">
                    Username<b style={{ color: "red" }}>*</b>
                  </span>
                </label>
                <Input
                  type="text"
                  id="username"
                  placeholder="Choose a username"
                  className="form-control"
                  value={username ?? ""}
                  status={errors.username ? "error" : undefined}
                  {...register("username", {
                    required: true,
                    onChange: (e) => handleChange("username", e.target.value),
                  })}
                />
              </div>
              <div className="form-group">
                <label htmlFor="role">
                  {" "}
                  <span className="details">
                    Role<b style={{ color: "red" }}>*</b>
                  </span>
                </label>
                <Input
                  type="text"
                  id="role"
                  placeholder="Assigned Role"
                  className="form-control"
                  value={role ?? ""}
                  status={errors.role ? "error" : undefined}
                  {...register("role", {
                    required: true,
                    onChange: (e) => handleChange("role", e.target.value),
                  })}
                />
              </div>
              <div className="form-group">
                <label htmlFor="location">
                  <span className="details">
                    Current Location<b style={{ color: "red" }}>*</b>
                  </span>
                </label>
                <Input
                  type="text"
                  id="location"
                  placeholder="Ex. Lagos, Nigeria."
                  className="form-control"
                  value={location ?? ""}
                  status={errors.location ? "error" : undefined}
                  {...register("location", {
                    required: true,
                    onChange: (e) => handleChange("location", e.target.value),
                  })}
                />
              </div>
              <div className="form-group">
                <label htmlFor="gender">
                  <span className="details">Gender</span>
                </label>
                <Select
                  style={{ width: "100%" }}
                  bordered={false}
                  allowClear
                  className="form-control select"
                  value={gender ?? ""}
                  placeholder={"-- select gender --"}
                  aria-label="Gender"
                  onChange={(value) => handleChange("gender", value)}
                >
                  <Option value="male">Male</Option>
                  <Option value="female">Female</Option>
                </Select>
              </div>
            </div>
            <div className="social-wrapper section">
              <div className="social-title">Social Links</div>
              <div className="social-links">
                <div className="form-group">
                  <label htmlFor="github">
                    <span className="details">GitHub Url</span>
                  </label>
                  <Input
                    type="text"
                    id="github"
                    placeholder="GitHub Profile"
                    className="form-control"
                    value={github ?? ""}
                    {...register("github", {
                      onChange: (e) => handleChange("github", e.target.value),
                    })}
                    addonBefore={selectBefore}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="gitlab">
                    <span className="details">Gitlab Url</span>
                  </label>
                  <Input
                    type="text"
                    id="gitlab"
                    placeholder="Gitlab Profile"
                    className="form-control"
                    value={gitlab ?? ""}
                    {...register("gitlab", {
                      onChange: (e) => handleChange("gitlab", e.target.value),
                    })}
                    addonBefore={selectBefore}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="gitlinked">
                    <span className="details">LinkedIn Url</span>
                  </label>
                  <Input
                    type="text"
                    id="gitlinked"
                    placeholder="LinkedIn Profile"
                    className="form-control"
                    {...register("linkedin", {
                      onChange: (e) => handleChange("linkedin", e.target.value),
                    })}
                    value={linkedin ?? ""}
                    addonBefore={selectBefore}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="twitter">
                    <span className="details">Twitter</span>
                  </label>
                  <Input
                    type="text"
                    id="twitter"
                    placeholder="Twitter"
                    className="form-control"
                    {...register("twitter", {
                      onChange: (e) => handleChange("twitter", e.target.value),
                    })}
                    value={twitter ?? ""}
                    addonBefore={selectBefore}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="instagram">
                    <span className="details">Instagram Url</span>
                  </label>
                  <Input
                    type="text"
                    id="instagram"
                    placeholder="instagram Profile"
                    className="form-control"
                    {...register("instagram", {
                      onChange: (e) =>
                        handleChange("instagram", e.target.value),
                    })}
                    value={instagram ?? ""}
                    addonBefore={selectBefore}
                  />
                </div>
              </div>
            </div>
            <div className="login-details-wrapper">
              <div className="login-title title">Creditials</div>
              <div className="login-details section">
                <div className="form-group">
                  <label htmlFor="phone">
                    {" "}
                    <span className="details">
                      USDT WalletAddress <b style={{ color: "red" }}>*</b>
                    </span>
                  </label>
                  <Input
                    type="text"
                    id="wallet-address"
                    placeholder="USDT wallet address"
                    className="form-control"
                    addonBefore={"USDT"}
                    value={usdtWalletAddress ?? ""}
                    status={errors.usdtWalletAddress ? "error" : undefined}
                    {...register("usdtWalletAddress", {
                      required: true,
                      onChange: (e) =>
                        handleChange("usdtWalletAddress", e.target.value),
                    })}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="phone">
                    {" "}
                    <span className="details">Phone</span>
                  </label>
                  <Input
                    type="text"
                    id="phone"
                    placeholder="Enter your phone no."
                    className="form-control"
                    addonBefore={PhoneCodes}
                    prefix={prefix}
                    inputMode={"tel"}
                    value={phone ?? ""}
                    status={errors.phone ? "error" : undefined}
                    {...register("phone", {
                      required: true,
                      onChange: (e) => handleChange("phone", e.target.value),
                    })}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="email">
                    {" "}
                    <span className="details">
                      Email<b style={{ color: "red" }}>*</b>
                    </span>
                  </label>
                  <Input
                    type="text"
                    id="email"
                    placeholder="Ex: tjdibbs@gmail.com"
                    className="form-control"
                    status={errors.email ? "error" : undefined}
                    {...register("email", {
                      required: true,
                      onChange: (e) => handleChange("email", e.target.value),
                    })}
                    value={email ?? ""}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="phone">
                    {" "}
                    <span className="details">
                      Password<b style={{ color: "red" }}>*</b>
                    </span>
                  </label>
                  <Input.Password
                    id="password"
                    placeholder="Enter password for logging in"
                    className="form-control"
                    status={errors.password ? "error" : undefined}
                    {...register("password", {
                      required: true,
                      onChange: (e) => handleChange("password", e.target.value),
                    })}
                    value={password ?? ""}
                  />
                </div>
              </div>
            </div>
            <div className="save-btn">
              <Button
                type="primary"
                size="large"
                loading={creating}
                htmlType="submit"
                block
                disabled={creating}
                className="save btn action-btn"
              >
                {creating ? "Creating profile ..." : "Create"}
              </Button>
            </div>
          </form>
        </div>
      </main>
    </Drawer>
  );
}
