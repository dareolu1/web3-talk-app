import React from "react";
import { Button, Typography, Card } from "antd";
import ConnectWalletModal from "./header/ConnectWalletModal";

export default function NotConnected() {
  const [isAuthModalVisible, setIsAuthModalVisible] = React.useState(false);
  return (
    <>
      <Card
        style={{ margin: "2em auto", padding: "2em 1em", borderRadius: "10px" }}
      >
        <Typography.Title level={5} style={{ marginBottom: "1em" }}>
          You need to connect your wallet to access the App
        </Typography.Title>
        <Button
          onClick={() => setIsAuthModalVisible(true)}
          style={{
            borderColor: "var(--primary-color)",
            borderRadius: "20px",
            color: "var(--primary-color)",
          }}
        >
          Connect Wallet
        </Button>
      </Card>
      {isAuthModalVisible && (
        <ConnectWalletModal setIsAuthModalVisible={setIsAuthModalVisible} />
      )}
    </>
  );
}
