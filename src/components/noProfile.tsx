import React from "react";
import { Button, Typography, Card } from "antd";
import CreateProfile from "./header/createProfile";

export default function NoProfile() {
  const [isAuthDrawerVisible, setIsAuthDrawerVisible] = React.useState(false);
  return (
    <>
      <Card
        style={{ margin: "2em auto", padding: "2em 1em", borderRadius: "10px" }}
      >
        <Typography.Title level={5} style={{ marginBottom: "1em" }}>
          You need to create profile to explore the features of the app
        </Typography.Title>
        <Button
          onClick={() => setIsAuthDrawerVisible(true)}
          style={{
            borderColor: "var(--primary-color)",
            borderRadius: "20px",
            color: "var(--primary-color)",
          }}
        >
          Create a profile
        </Button>
      </Card>
      <CreateProfile
        isAuthDrawerVisible={isAuthDrawerVisible}
        setIsAuthDrawerVisible={setIsAuthDrawerVisible}
      />
    </>
  );
}
